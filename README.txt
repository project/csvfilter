
This filter allow you to insert tables in CSV format into your posts.
Tables shows as html-tables. Tags <table> <tr> and <td> must be enabled
in html-filter of your site.

You mas assign custom styles to rows and columns separately for every 
posted table.

CSV-data must be placed between [CSV]...[/CSV] tags. Additional parameters
may be used in first tag. See example above:

[CSV unquoted sep=; sortby=1]
поле №1;поле №2;поле №3
1;Строка 1;1.2
3;Строка 3;тест
2;Строка 2;3.2
[/CSV]

Full list of additional parameters see in module help. By default filter
accept comma separated strings enquoted with '"'.

Author - Axel <axel@drupal.ru>
License - GPL (see LICENSE.txt)
Development of csvfilter for Drupal 4.4 was sponsored by Andrey Pshenichny (sunrobot.com)
