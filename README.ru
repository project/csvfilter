
// This text in Russian in UTF-8

Этот фильтр дает возможность вставки таблиц в формате CSV в ваши документы.

Автор - Аксель <axel@drupal.ru>
Лицензия - GPL (см. LICENSE)
Разработку csvfilter для Drupal 4.4 спонсировал Андрей Пшеничный.
Поддержка для русских пользователей - http://drupal.ru/forum/support

